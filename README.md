#  Blender-EduTech - Tutoriels

Ce dépôt regroupe différents tutoriels pour découvrir l'environnement de développement [Blender](https://blender.org)+[UPBGE](https://upbge.org)+[Python](https://python.org).

Les fichiers sources sont dans le projet **Blender-Edutech /  Tutorials** de La Forge Codeberg : https://codeberg.org/Blender-EduTech/Tutorials .

## Documentation générale

Les tutoriels n'aborderont pas dans les détails ni l'utilisation de Blender ni le codage en Python. 

Pour Blender + UPBGE, la documentation officielle est : 
- [Manuel de référence de Blender](https://docs.blender.org/manual/en/latest/)
- [Manuel de référence de UPBGE](https://upbge.org/manual/index.html#/documentation/docs/latest/manual/index.html)
- [API UPBGE](https://upbge.org/manual/index.html#/documentation/docs/latest/api/index.html)

Un des livres de référence pour Blender en Français est ["La 3D libre avec Blender" (Éditions Eyrolles)](https://www.eyrolles.com/Audiovisuel/Livre/la-3d-libre-avec-blender-9782416005817/) de Olivier Saraja, Henri Hebeisen et Boris Fauret.

## [Labyrinthe à bille](https://codeberg.org/Blender-EduTech/Tutorials/src/branch/main/labyrinthe)

- Tutoriel 1 : **Créer une scène 3D interactive**,
- Tutoriel 2 : **Programmation en Python**,
- Tutoriel 3 : **Interfaçage avec une carte Arduino par le protocole pyFirmata**,
- Tutoriel 4 : **Interfaçage avec une carte Arduino par la liaison série**,
- Tutoriel 5 : **Interfaçage avec une carte micro:bit**,
- Tutoriel 6 : **Développement d'un jumeau numérique**.
<!-- - Tutoriel 7 : **Commande par machine learning**. -->

