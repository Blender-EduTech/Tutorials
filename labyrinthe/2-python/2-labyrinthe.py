import bge # Bibliothèque Blender Game Engine (BGE)

###############################################################################
# 2-labyrinthe.py
# @title: Module (unique) de la scène 3D du labyrinthe à bille
# @project: Blender-EduTech - Tutoriel 2 : Labyrinthe à bille - Passage au Python
# @lang: fr
# @authors: Philippe Roy <philippe.roy@ac-grenoble.fr>
# @copyright: Copyright (C) 2023 Philippe Roy
# @license: GNU GPL
#
# Commandes déclenchées par UPBGE pour le scène du labyrinthe
#
###############################################################################

# Récupérer la scène 3D
scene = bge.logic.getCurrentScene()
# print("Objets de la scene : ", scene.objects) # Lister les objets de la scène

# Constantes

JUST_ACTIVATED = bge.logic.KX_INPUT_JUST_ACTIVATED
JUST_RELEASED = bge.logic.KX_INPUT_JUST_RELEASED
ACTIVATE = bge.logic.KX_INPUT_ACTIVE

###############################################################################
# Gestion du clavier
###############################################################################

# Flèches pour tourner le plateau
def  clavier(cont):
    obj = cont.owner # obj est l'objet associé au contrôleur donc 'Plateau'
    # obj = scene.objects['Plateau']
    keyboard = bge.logic.keyboard
    resolution = 0.01

    # Flèche haut - Up arrow
    if keyboard.inputs[bge.events.UPARROWKEY].status[0] == ACTIVATE:
        obj.applyRotation((-resolution,0,-obj.worldOrientation.to_euler().z), False)
    
    # Flèche bas - Down arrow
    if keyboard.inputs[bge.events.DOWNARROWKEY].status[0] == ACTIVATE:
        obj.applyRotation((resolution,0,-obj.worldOrientation.to_euler().z), False)
 
    # Flèche gauche - Left arrow
    if keyboard.inputs[bge.events.LEFTARROWKEY].status[0] == ACTIVATE:
        obj.applyRotation((0, -resolution,-obj.worldOrientation.to_euler().z), False)

    # Flèche droit - Right arrow
    if keyboard.inputs[bge.events.RIGHTARROWKEY].status[0] == ACTIVATE:
        obj.applyRotation((0, resolution, -obj.worldOrientation.to_euler().z), False)

###############################################################################
# Gameplay
###############################################################################

# Initialisation de la scène
def  init(cont):
    obj = cont.owner # obj est l'objet associé au contrôleur donc 'Bille'

    # Mémorisation de la position de départ de la bille
    obj['init_x']=obj.worldPosition.x
    obj['init_y']=obj.worldPosition.y
    obj['init_z']=obj.worldPosition.z

    # Cacher le panneau de la victoire et suspendre la physique du panneau cliquable
    scene.objects['Panneau victoire'].setVisible(False,True)
    scene.objects['Panneau victoire - plan'].suspendPhysics (True)
    scene.objects['Bouton fermer'].color = (0, 0, 0, 1) # Noir

# Cycle (boucle de contrôle de la bille)
def  cycle(cont):
    obj = cont.owner # obj est l'objet associé au contrôleur donc 'Bille'
    obj['z']=obj.worldPosition.z # la propriété z est mis à jour avec la position globale en z de la bille

    # Si l'altitude de bille < -10 et pas de victoire -> chute
    if obj['z'] < -10 and obj['victoire'] == False:
        print ("Chuuuu.....te")
        depart() # Replacer la bille au départ

# Départ de la bille
def  depart():
    obj_bille = scene.objects['Bille']
    obj_plateau = scene.objects['Plateau']

    # Replacement du plateau (tous les angles à 0 en plusieurs fois)
    while obj_plateau.worldOrientation.to_euler().x != 0 and obj_plateau.worldOrientation.to_euler().y !=0 and obj_plateau.worldOrientation.to_euler().z !=0 :
        obj_plateau.applyRotation((-obj_plateau.worldOrientation.to_euler().x, -obj_plateau.worldOrientation.to_euler().y, -obj_plateau.worldOrientation.to_euler().z), False)

    # Mettre la bille à la position de départ avec une vitesse nulle
    obj_bille = scene.objects['Bille']
    obj_bille.worldLinearVelocity=(0, 0, 0)
    obj_bille.worldAngularVelocity=(0, 0, 0)
    obj_bille.worldPosition.x = obj_bille['init_x']
    obj_bille.worldPosition.y = obj_bille['init_y']
    obj_bille.worldPosition.z = obj_bille['init_z']+0.5 # On repose la bille
    obj_bille['victoire']=False
    obj_bille['chute'] = False

# Victoire (collision de la bille avec l'arrivée)
def victoire(cont):
    scene.objects['Bille']['victoire']=True
    scene.objects['Panneau victoire'].setVisible(True,True) # Afficher le panneau de la victoire
    scene.objects['Panneau victoire - plan'].restorePhysics() # Restaurer la physique du panneau cliquable
    start = 1
    end = 100
    layer = 0
    priority = 1
    blendin = 1.0
    mode = bge.logic.KX_ACTION_MODE_PLAY
    layerWeight = 0.0
    ipoFlags = 0
    speed = 1
    scene.objects['Panneau victoire'].playAction('Panneau victoireAction', start, end, layer, priority, blendin, mode, layerWeight, ipoFlags, speed)

# Highlight du bouton Fermer
def victoire_fermer_hl(cont):
    obj = cont.owner

    # Activation
    if cont.sensors['MO'].status == JUST_ACTIVATED:
            obj.color = (1, 1, 1, 1) # Blanc

    # Désactivation
    if cont.sensors['MO'].status == JUST_RELEASED:
        obj.color = (0, 0, 0, 1) # Noir

# Fermer le panneau de la victoire (clic)
def victoire_fermer(cont):
    if cont.sensors['Click'].status == JUST_ACTIVATED and cont.sensors['MO'].positive:
        scene.objects['Panneau victoire'].setVisible(False,True) # Cacher le panneau de la victoire
        scene.objects['Panneau victoire - plan'].suspendPhysics (True) # Suspendre la physique du panneau cliquable
        depart() # Replacer la bille au départ

###############################################################################    
# Gestion du Joystick (Joystick USB)
# Partie non abordée dans le tutoriel
###############################################################################

def  joystick(cont):
    obj = cont.owner
    joystickIndex = 0 #int from 0 to 6
    joy = bge.logic.joysticks[joystickIndex]
    events = joy.activeButtons
    axis = joy.axisValues[0:4]
    resolution = 0.01
    
    leftStick_x = axis[0]; leftStick_y = axis[1]
    rightStick_x = axis[2]; rightStick_y = axis[3]

    #if any button is pressed
    # if events:
    #     print(events) #spit out integer index of pressed buttons
    # if 0 in events:
    #     doSomething()
        
    # Up
    if leftStick_y <-0.1 :
        obj.applyRotation((-resolution,0,0), False)
    
    # Down
    if leftStick_y >0.1 :
        obj.applyRotation((resolution,0,0), False)
 
    # Left
    if leftStick_x <-0.1 :
        obj.applyRotation((0, -resolution,0), False)

    # Right
    if leftStick_x >0.1 :
        obj.applyRotation((0, resolution,0), False)
