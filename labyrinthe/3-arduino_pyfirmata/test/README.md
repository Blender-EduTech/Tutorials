## Labyrinthe à bille : **Créer une scène 3D interactive et interfacée avec un microcontroleur**

### Programmes de test  du Tutoriel 3 (Interfacage scène 3D <-> carte Arduino par pyFirmata) :

- pyfirmata-test.py : Test de validation de la communication entre la carte Arduino et l'ordinateur en passant par le protocol Firmata
- manette-test.py : Test de la manette Grove constituée de 4 boutons et d'1 joystick
