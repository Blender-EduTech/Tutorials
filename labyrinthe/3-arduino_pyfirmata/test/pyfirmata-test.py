import pyfirmata
import time
import signal
import sys

###############################################################################
# pyfirmata-test.py :
# @title: Test de l'environnement Python pour Arduino via pyFirmata
# @project: 
# @lang: fr
# @authors: Philippe Roy <philippe.roy@ac-grenoble.fr>
# @copyright: Copyright (C) 2021-2022 Philippe Roy
# @license: GNU GPL
#
# Ce programme est un programme test pour vérifier si l'environnement Python pour Arduino est opérationnel.
# Il y a deux conditions pour son fonctionnement :
# - le programme de la carte Arduino doit être 'StandardFirmata',
# - la bibliothèque Python 'pyFirmata' doit être installée sur l'ordinateur.
#
# Il faut que la led de la carte Arduino clignote.
# 
###############################################################################

# Explication dans la console
print("Ce programme est un programme test pour vérifier si l'environnement Python pour Arduino est opérationnel.")
print("Il faut que la led de carte Arduino clignote.")
print("")
print("Il y a deux conditions pour son fonctionnement :")
print("- le programme de la carte Arduino doit être 'StandardFirmata',")
print("- la bibliothèque Python 'pyFirmata' doit être installée sur l'ordinateur.")
print("")

# Connexion à la carte Arduino
#carte = pyfirmata.Arduino('/dev/ttyACM0')
carte = pyfirmata.Arduino('COM4')
print("Communication Carte Arduino établie")

# Itérateur pour les entrées
it = pyfirmata.util.Iterator(carte)
it.start()

# Définition entrées-sorties
led = carte.get_pin('d:13:o')

# Fermer proprement la communication avec la carte Arduino lors de l'arrêt du programme
def sigint_handler(signal, frame):
    carte.exit()
    time.sleep(1)
    sys.exit(0)
    
# Boucle principale
while True:
    led.write(1)
    print("Led carte allumée")
    time.sleep(1)
    
    led.write(0)
    print("Led carte éteinte")
    time.sleep(1)

    # Capture de l'interruption du programme
    signal.signal(signal.SIGINT, sigint_handler)

    
