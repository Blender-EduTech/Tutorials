import pyfirmata # Communication carte
import time
import signal # Interruptions systeme
import sys

###############################################################################
# manette-test.py :
# @title: Test de la manette 4 boutons Grove et 1 joystick Grove en passant par le protocol Firmata
# @project: Blender-EduTech - Tutoriel : Tutoriel 3 Labyrinthe à bille - Interfacer la scène 3D avec une carte Arduino
# @lang: fr
# @authors: Philippe Roy <philippe.roy@ac-grenoble.fr>
# @copyright: Copyright (C) 2023 Philippe Roy
# @license: GNU GPL
#
# Commandes simples avec une manette 4 boutons et un joystick
# 
###############################################################################

###############################################################################
# Communication avec la carte Arduino
###############################################################################

# carte = pyfirmata.Arduino('COM4') # Windows
carte = pyfirmata.Arduino('/dev/ttyACM0') # GNU/Linux
print("Communication Carte Arduino établie")

# Itérateur pour les entrees
it = pyfirmata.util.Iterator(carte)
it.start()

# Fermer proprement la communication avec la carte Arduino lors de l'arret du programme
def sigint_handler(signal, frame):
    print ('Interruption du programme capturée.')
    carte.exit()
    time.sleep(1)
    sys.exit(0)

###############################################################################
# Définition entrées - sorties
###############################################################################

# Définition des 4 boutons
bt_a = carte.get_pin('d:2:i')
bt_r = carte.get_pin('d:3:i')
bt_g = carte.get_pin('d:4:i')
bt_d = carte.get_pin('d:5:i')

# Définition du joystick
jstk_x = carte.get_pin('a:0:i')
jstk_y = carte.get_pin('a:1:i')

# led = carte.get_pin('d:13:o')

###############################################################################
# Boucle principale
###############################################################################

while True:
    
    ##
    # 4 boutons
    ##
    
    if bt_a.read() == True and bt_r.read() == False:
        print("Bouton haut")
    if bt_a.read() == False and bt_r.read() == True:
        print("Bouton bas")
    if bt_g.read() == True and bt_d.read() == False:
        print("Bouton gauche")
    if bt_g.read() == False and bt_d.read() == True :
        print("Bouton droit")

    ##
    # Joystick
    ##

    print("Joystick X : ", jstk_x.read())
    print("Joystick Y : ", jstk_y.read())

    ##
    # Cadencement
    ##
    
    time.sleep(0.1)

    ##
    # Capture de l'interruption du programme
    ##
    
    signal.signal(signal.SIGINT, sigint_handler)
