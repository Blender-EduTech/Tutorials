from microbit import uart, sleep
from microbit import *

###############################################################################
# 5-labyrinthe-microbit.py
# @title: Programme pour la carte micro:bit de gestion de la centrale inertielle
# @project: Blender-EduTech - Tutoriel 5 : Labyrinthe à bille - Interfacer avec une carte micro:bit
# @lang: fr
# @authors: Philippe Roy <philippe.roy@ac-grenoble.fr>
# @copyright: Copyright (C) 2023 Philippe Roy
# @license: GNU GPL
#
# Commandes déclenchées par UPBGE pour le scène du labyrinthe
#
###############################################################################

###############################################################################
# Initialisation
###############################################################################

attente_image = Image("00000:00000:00300:00000:00000")
display.show(attente_image)  # Témoin de fonctionnement

uart.init(baudrate= 115200) # Initialisation du port série

###############################################################################
# Boucle principale
###############################################################################

msg_str=''

while True:

    # A propos
    if button_a.is_pressed() or button_b.is_pressed():
        display.scroll("Labyrinte")
        display.show(attente_image) # Témoin de fonctionnement
        
    # Inclinaison de la carte -> UBGE
    accel_x=accelerometer.get_x() # Roulis
    accel_y=accelerometer.get_y() # Tangage
    uart.write(str(accel_x)+","+str(accel_y)+"\n")        

    # UBGE -> micro:bit (lecture du message)
    msg_byte = uart.readline()
    if msg_byte:
        display.clear()
        msg_str = str(msg_byte, 'ascii')
        if "91" in msg_str: # Chute
            display.show(Image.SAD)
            sleep(500);
            uart.write("start\n")
        elif "92" in msg_str: # Victoire
            display.show(Image.HAPPY)
        else: # Position de la bille
            display.set_pixel(int(msg_str[0]), int(msg_str[1]), 9)
    
    sleep(100)
