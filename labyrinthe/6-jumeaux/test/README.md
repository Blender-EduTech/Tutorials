## Labyrinthe à bille : **Créer une scène 3D interactive et interfacée avec un microcontroleur**

### Programmes de test  du Tutoriel 6 (Développement de jumeau numérique) :

- servo-test.ino : Commande simple du servomoteur
- servo-test.py : Commande simple du servomoteur via pyFirmata
- manette-test.ino : Commande des servomoteurs à la manette Grove (4 boutons + joystick) avec retour à l'horizontale via le capteur inertiel (IMU)
- cam_bille-test.py : Détection de la bille par vision (caméra + OpenCV)
