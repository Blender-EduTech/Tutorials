import pyfirmata

###############################################################################
# servo-test.py :
# @title:  Test de pilotage d'un servomoteur par pyFirmata
# @project: Blender-EduTech - Tutoriel : Tutoriel 6 : Labyrinthe à bille - Développement de jumeau numérique
# @lang: fr
# @authors: Philippe Roy <philippe.roy@ac-grenoble.fr>, Andre Nicolas <Nicolas.Andre@ac-grenoble.fr>
# @copyright: Copyright (C) 2023 Philippe Roy
# @license: GNU GPL
###############################################################################

# Communication avec la carte Arduino
# carte = pyfirmata.Arduino('COM3') # Windows
carte = pyfirmata.Arduino('/dev/ttyACM1') # GNU/Linux
print("Communication Carte Arduino établie")

servo = carte.get_pin('d:9:s') # Servomoteur sur la broche D9

try :
    while True: # Boucle infinie
        print ("Angle :", servo.read())
        angle = int(input("Entrer un angle entre 10 et 170) : ")) # Demande à l’utilisateur d’entrer la valeur de l’angle
        if angle < 10 : # Si la valeur entrée est inférieure à 10° on bloque le servomoteur à 10°
            angle = 10
        elif angle > 170 : # Si la valeur entrée est supérieure à 170° on bloque le servomoteur à 170°
            angle = 170
        servo.write(angle) # Ecrit cette valeur sur la broche du servomoteur
except :
    carte.exit()
