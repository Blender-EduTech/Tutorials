## Labyrinthe à bille : **Créer une scène 3D interactive et interfacée avec un microcontroleur**

L'objectif de ce tutoriel est de créer une scène 3D interactive numériquement (éléments sensibles de la scène 3D) et physiquement par des capteurs (via une
interface avec un microcontroleur). Le support est le labyrinthe à bille ; le principe est faire tourner le plateau sur 2 axes afin d'amener la bille du départ
à l'arrivée.

Ce tutoriel est une déclinaison pour UPBGE du projet n°1 du livre ["Créez vos propres jeux 3D comme les pros" (Éditions Graziel)](https://graziel.com/fr/livres/8-creez-vos-propres-jeux-3d-comme-les-pros-avec-le-blender-game-engine-9791093846002.html) de Grégory Gossellin De Bénicourt.

La version de Blender/UPBGE utilisée lors du développement de ce tutoriel est la version 0.36.1 (20 août 2023). Le modèle CAO (SolidWorks, FreeCAD et STEP) est présent dans le dépot du modède 3D : https://codeberg.org/Blender-EduTech/Ball_balancing

Le tutoriel se décompose en 6 parties (les deux premières sont obligatoires et séquentielles) :

![titres](img/labyrinthe-titres.jpg)

### [Tutoriel 1 : Ma première scène](https://codeberg.org/Blender-EduTech/Tutorials/src/branch/main/labyrinthe/1-scene3D)
- Installer Blender/UPBGE
- Modéliser les objets 3D et définir leurs materiaux et leur physique
- Gérer la scène avec la lumière et la caméra
- Programmer le comportement des objets et le gameplay les briques logiques
- Créer une zone cliquable
- Créer une animation par images-clé
- Produire un exécutable (GNU/Linux, Windows, macOS)
- **Fichier résultat : 1-labyrinthe.blend**

### [Tutoriel 2 : Passage au Python](https://codeberg.org/Blender-EduTech/Tutorials/src/branch/main/labyrinthe/2-python)
- Installer un éditeur de code (Emacs, Spyder)
- Sustituer la programmation par briques logiques avec des modules codés en Python
- Créer un bouton cliquable à partir une icône SVG
- Inclure les scripts Python avec l'exécutable
- **Fichier de départ : 2-labyrinthe-debut.blend**
- **Fichier ressource : asset/icon_close.svg**
- **Fichiers résultats : 2-labyrinthe.blend,  2-labyrinthe.py**

### [Tutoriel 3 : Interfacer la scène 3D avec une carte Arduino par pyFirmata](https://codeberg.org/Blender-EduTech/Tutorials/src/branch/main/labyrinthe/3-arduino_pyfirmata)
- Installer la bibliothèque pyFirmata
- Déplacer le plateau avec une manette : 4 boutons binaires et/ou un joystick analogique
- Allumer une led quand le plateau est en mouvement
- Inclure la bibliothèque pyFirmata avec l'exécutable
- **Fichiers de départ : 2-labyrinthe.blend, 2-labyrinthe.py**
- **Documents techniques : Carte de référence pyFirmata et interface Grove pour Arduino**
- **Fichiers résultats : 3-labyrinthe.blend,  3-labyrinthe.py, labyrinthe_carte.py**

### [Tutoriel 4 : Interfacer la scène 3D avec une carte Arduino par pySerial](https://codeberg.org/Blender-EduTech/Tutorials/src/branch/main/labyrinthe/4-arduino_pyserial)
- Installer la bibliothèque pySerial
- Déplacer le plateau avec une centrale inertielle (capteur IMU sur broche I2C)
- Afficher la position de la bille sur une matrice de leds (broche I2C)
- Inclure la bibliothèque pySerial avec l'exécutable
- **Fichiers de départ : 2-labyrinthe.blend, 2-labyrinthe.py**
- **Document technique : Interface Grove pour Arduino**
- **Fichiers résultats : 4-labyrinthe.blend, 4-labyrinthe.py, 4-labyrinthe-imu.ino, labyrinthe_carte.py**
<!-- - Déplacer le plateau avec un détecteur de geste ; fichiers résultats : 3-labyrinthe-gest.blend,  3-labyrinthe-gest.py, 3-labyrinthe-gest.ino -->

### [Tutoriel 5 : Interfacer la scène 3D avec une carte micro:bit](https://codeberg.org/Blender-EduTech/Tutorials/src/branch/main/labyrinthe/5-microbit)
- Installer la bibliothèque pySerial
- Déplacer le plateau avec la centrale inertielle de la carte microbit
- Afficher la position de la bille sur la matrice de leds
- Inclure la bibliothèque pySerial avec l'exécutable
- **Fichiers de départ : 2-labyrinthe.blend, 2-labyrinthe.py**
- **Document technique : Carte de référence carte micro:bit**
- **Fichiers résultats : 5-labyrinthe.blend,  5-labyrinthe.py, 5-labyrinthe-microbit.py, labyrinthe_carte.py**

### [Tutoriel 6 : Développer le jumeau numérique du labyrinthe (en cours d'écriture)](https://codeberg.org/Blender-EduTech/Tutorials/src/branch/main/labyrinthe/6-jumeaux)
- Imprimer et assembler le labyrinthe
- Commander manuellemet le labyrinthe physique
- Suivre la bille réelle par OpenCV (par vision)
- Caler le moteur physique Bullet avec le labyrinthe physique
- Commander le labyrinthe physique par pathfinding + vision le labyrinthe physique
- **Fichiers de départ : 4-labyrinthe.blend, 4-labyrinthe.py, 4-labyrinthe-imu.ino, labyrinthe_carte.py**
- **Fichiers résultats : 6-labyrinthe.blend, 6-labyrinthe.py, 6-labyrinthe-arduino.ino, labyrinthe_carte.py**
