## Bibliothèques pour Arduino

### Grove_IMU_9DOF :
- Wiki : https://wiki.seeedstudio.com/Grove-IMU_9DOF_v2.0/
- Forge : https://github.com/Seeed-Studio/Grove_IMU_9DOF
- Licence : MIT

### Grove_LED_Matrix_Driver_HT16K33 :
- Wiki : https://wiki.seeedstudio.com/Grove-Red_LED_Matrix_w_Driver
- Forge : https://github.com/Seeed-Studio/Grove_LED_Matrix_Driver_HT16K33
- Outil en ligne pour générer des caractères : https://xantorohara.github.io/led-matrix-editor/#
- Licence : MIT

