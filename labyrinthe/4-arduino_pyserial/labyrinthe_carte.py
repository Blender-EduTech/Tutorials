import serial # Liaison série
from serial.tools.list_ports import comports # Détection du port automatique

###############################################################################
# labyrinthe_carte.py
# @title: Détection automatique de la carte Arduino ou microbit
# @project: Blender-EduTech - Tutoriel 4 : Labyrinthe à bille - Interfacer avec une carte Arduino par la liaison série
# @lang: fr
# @authors: Philippe Roy <philippe.roy@ac-grenoble.fr>
# @copyright: Copyright (C) 2023 Philippe Roy
# @license: GNU GPL
#
# Détection automatique de la carte Arduino ou microbit
#
###############################################################################

###############################################################################
# Communication avec la carte Arduino
###############################################################################

# Recherche automatique du port (microbit, Arduino Uno et Arduino Mega)
def autoget_port():
    # USB Vendor ID,  USB Product ID
    carte_dict={'microbit' :[3368, 516],
        'uno' :[9025, 67],
        'mega' :[9025, 66]}
    for com in comports(): # micro:bit
        if com.vid == carte_dict['microbit'][0] and com.pid == carte_dict['microbit'][1]:
            return [com.device,"micro:bit"]
    for com in comports(): # Arduino Uno
        if com.vid == carte_dict['uno'][0] and com.pid == carte_dict['uno'][1]:
            return [com.device,"Arduino Uno"]
    for com in comports(): # Arduino Mega
        if com.vid == carte_dict['mega'][0] and com.pid == carte_dict['mega'][1]:
            return [com.device,"Arduino Mega"]
    return [None,""]

 # Établir la communication avec la carte par la liaison série avec une vitesse
def init_serial(speed=115200):
    [port, carte_name] =autoget_port()
    print (port, carte_name)
    if port is None:
        print("Carte Arduino/microbit introuvable")
        return None
    else:
        serial_comm = serial.Serial(port, speed, timeout=0.016)
        if serial_comm is None:
            print("Communication avec Carte Arduino/microbit impossible")
            return None
        else:
            print("Communication avec Carte Arduino/microbit établie sur "+port+" à la vitesse "+str(speed)+" bauds")
            return serial_comm
